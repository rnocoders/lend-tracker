package com.rnocoders.lendtracker;

import com.rnocoders.lendtracker.main.BaseController;
import com.rnocoders.lendtracker.main.ProjectDetailsEntity;
import com.rnocoders.lendtracker.main.ProjectDetailsRepository;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@SpringBootTest
class BaseControllerTest {

    @InjectMocks
    private BaseController baseController;

    @Mock
    private ProjectDetailsRepository projectDetailsRepository;

    @Test
    void projectDetailsTest(){
        //Mock Data
        List<ProjectDetailsEntity> projectDetailsMockData = new ArrayList<>();
        ProjectDetailsEntity projectDetailsEntity = new ProjectDetailsEntity();
        projectDetailsEntity.setName("project_name");
        projectDetailsEntity.setValue("lend_tracker");
        projectDetailsMockData.add(projectDetailsEntity);

        //Mocking
        when(projectDetailsRepository.findAll()).thenReturn(projectDetailsMockData);

        //Assert
        assertEquals("lend_tracker",baseController.projectDetails().get(0).getValue());
    }
}
