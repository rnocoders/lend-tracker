package com.rnocoders.lendtracker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LendTrackerApplication {

	public static void main(String[] args) {
		SpringApplication.run(LendTrackerApplication.class, args);
	}

}
