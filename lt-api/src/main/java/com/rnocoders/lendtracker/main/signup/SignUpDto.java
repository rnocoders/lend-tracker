package com.rnocoders.lendtracker.main.signup;

import java.io.*;
import lombok.*;

import javax.validation.constraints.*;

@Data
public class SignUpDto implements Serializable {

    @NotBlank(message = "userName should not be Empty")
    private String userName;

    @NotBlank(message = "Password should not be empty")
    private String password;
    private String email;
}
