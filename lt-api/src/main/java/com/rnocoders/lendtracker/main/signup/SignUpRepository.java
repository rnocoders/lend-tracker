package com.rnocoders.lendtracker.main.signup;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.*;
import org.springframework.web.bind.annotation.*;

@Repository
public interface SignUpRepository extends JpaRepository<SignUp, Long> {
}