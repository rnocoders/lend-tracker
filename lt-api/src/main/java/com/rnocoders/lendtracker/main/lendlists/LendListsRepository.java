package com.rnocoders.lendtracker.main.lendlists;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LendListsRepository extends JpaRepository<LendListsEntity, Integer> {
}

@Repository
interface BorrowerInformationRepository extends JpaRepository<BorrowerInformation, Integer>{

}
