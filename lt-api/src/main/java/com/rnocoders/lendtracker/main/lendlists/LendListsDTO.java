package com.rnocoders.lendtracker.main.lendlists;

import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;
import java.time.LocalDateTime;

@Getter
@Setter
public class LendListsDTO {
        private int lendListsId;
        private int userId = 0;
        private int borrowerId;
        private String itemName;
        private String itemDescription;
        private LocalDateTime lendDate;
        private LocalDateTime dueDate;
        private LocalDateTime returnDate;
        private LocalDateTime createdDate;
        private int statusId;
        private String category;
        private Timestamp timestamp;
}

@Getter
@Setter
class StatusDTO{
    private int statusId;
    private String status;
}

@Getter
@Setter
class BorrowerInformationDTO{
    private int borrowerId;
    private String borrowerName;
    private String phoneNumber;
    private String email;
}
