package com.rnocoders.lendtracker.main.signup;

import com.rnocoders.lendtracker.shared.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import javax.validation.*;

@RestController
@RequestMapping("/api/v0")
public class SignUpController {

    @Autowired
    private SignUpService signUpService;

    @PostMapping("/addUserDetails")
    public ResponseEntity<ExpectedReturnDto<SignUpDto>> addUserDetails(@Valid @RequestBody SignUpDto userDto){

       return ResponseEntity.ok(signUpService.addUserDetails(userDto));
    }
}
