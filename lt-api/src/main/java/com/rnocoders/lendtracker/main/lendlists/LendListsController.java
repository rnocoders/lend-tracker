package com.rnocoders.lendtracker.main.lendlists;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class LendListsController {

    private static final Mapper mapper = new DozerBeanMapper();

    @Autowired
    private LendListsService lendListsService;

    @GetMapping("/find-all-lists")
    public List<LendListsDTO> findAllLists(){
        List<LendListsEntity> lendListsEntities = lendListsService.findAllLists();
        return lendListsEntities.stream().map(list -> mapper.map(list, LendListsDTO.class)).collect(Collectors.toList()) ;
    }

    @Transactional
    @PostMapping("/add-lend")
    public void addLend(@RequestBody LendListsDTO lendLitsDTO){
        LendListsEntity lendListsEntity = mapper.map(lendLitsDTO, LendListsEntity.class);
        lendListsService.addLend(lendListsEntity);
    }
}
