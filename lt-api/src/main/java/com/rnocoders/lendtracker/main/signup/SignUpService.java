package com.rnocoders.lendtracker.main.signup;

import com.rnocoders.lendtracker.shared.*;
import org.springframework.stereotype.*;

@Service
public interface SignUpService {
    ExpectedReturnDto<SignUpDto> addUserDetails(SignUpDto userDto) throws DuplicateUserEntryException;
}
