package com.rnocoders.lendtracker.main.lendlists;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Service
public class LendListsServiceImpl implements LendListsService{

    @Autowired
    LendListsRepository lendListsRepository;

    @Autowired
    BorrowerInformationRepository borrowerInformationRepository;

    @Override
    public List<LendListsEntity> findAllLists(){
        return lendListsRepository.findAll();
    }

    @Override
    public void addLend(@RequestBody LendListsEntity lendListsEntity){
        lendListsRepository.save(lendListsEntity);
    }

}
