package com.rnocoders.lendtracker.main.signup;

import com.sun.istack.*;
import java.io.*;
import java.util.*;
import lombok.*;

import javax.persistence.*;

@Table(name = "user_registry")
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SignUp implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @NotNull
    @Column(name = "user_name", unique = true)
    private String userName;

    @Column(name = "password", unique = true, nullable = false)
    private String password;

    @Column(name = "user_email_Id", unique = true)
    private String email;

    @Temporal(TemporalType.TIMESTAMP)
    private Date lastLogin;


}
