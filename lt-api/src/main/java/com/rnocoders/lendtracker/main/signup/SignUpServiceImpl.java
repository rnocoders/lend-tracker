package com.rnocoders.lendtracker.main.signup;

import com.rnocoders.lendtracker.shared.*;
import java.util.*;
import org.dozer.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.security.crypto.bcrypt.*;
import org.springframework.stereotype.*;
import org.springframework.transaction.annotation.*;

@Service
public class SignUpServiceImpl implements SignUpService {

    private static final Mapper mapper = new DozerBeanMapper();

    @Autowired
    private SignUpRepository signUpRepository;

    @Override
    @Transactional
    public ExpectedReturnDto<SignUpDto> addUserDetails(SignUpDto userDto) throws DuplicateUserEntryException {

        List<SignUp> signUpList = signUpRepository.findAll();

        boolean b = signUpList.stream().anyMatch(user -> userDto.getUserName().equals(user.getUserName()));

        if(b) {
            throw new DuplicateUserEntryException("userName is already  Taken");
        }



        SignUp signUp = mapper.map(userDto, SignUp.class);
        signUp.setLastLogin(new Date());

        // encoding the password
        String encodedPassword = new BCryptPasswordEncoder().encode(signUp.getPassword());
        signUp.setPassword(encodedPassword);

        SignUp save = signUpRepository.save(signUp);
        SignUpDto returnDetails = mapper.map(save, SignUpDto.class);

        ExpectedReturnDto<SignUpDto> signUpDtoExpectedReturnDto = new ExpectedReturnDto<>();
        signUpDtoExpectedReturnDto.setStatus("User Details Has been added Successfully");
        signUpDtoExpectedReturnDto.setReturnObject(returnDetails);

        return signUpDtoExpectedReturnDto;
    }
}
