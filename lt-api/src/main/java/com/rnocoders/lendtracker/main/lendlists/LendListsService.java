package com.rnocoders.lendtracker.main.lendlists;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Service
public interface LendListsService {

    List<LendListsEntity> findAllLists();

    void addLend(@RequestBody LendListsEntity lendListsEntity);
}
