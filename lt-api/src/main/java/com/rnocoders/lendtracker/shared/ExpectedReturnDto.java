package com.rnocoders.lendtracker.shared;

import com.sun.source.doctree.*;
import java.io.*;
import lombok.*;
import org.springframework.stereotype.*;


@Data
public class ExpectedReturnDto<T> {

    private String status;

    private T returnObject;

}
