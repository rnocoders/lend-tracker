package com.rnocoders.lendtracker.shared;

public class DuplicateUserEntryException extends RuntimeException {

    public DuplicateUserEntryException() {
        super();
    }

    public DuplicateUserEntryException(String message) {
        super(message);
    }

    public DuplicateUserEntryException(String message, Throwable cause) {
        super(message, cause);
    }

    protected DuplicateUserEntryException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }

    @Override
    public synchronized Throwable getCause() {
        return super.getCause();
    }
}
