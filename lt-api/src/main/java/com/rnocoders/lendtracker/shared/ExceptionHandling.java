package com.rnocoders.lendtracker.shared;

import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.*;
import org.springframework.http.*;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

@RestControllerAdvice
public class ExceptionHandling {

    @Autowired
    private ErrorMessage errorMessage;

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }

    @ExceptionHandler(DuplicateUserEntryException.class)
    public ResponseEntity<ErrorMessage> printDuplicateErrorDetails(DuplicateUserEntryException duplicateUserEntryException ){

        errorMessage.setHttpStatus(HttpStatus.NOT_ACCEPTABLE);
        errorMessage.setStatusCode(HttpStatus.NOT_ACCEPTABLE.value());
        errorMessage.setMessage(duplicateUserEntryException.getMessage());
        return new ResponseEntity<>(errorMessage,HttpStatus.BAD_REQUEST);
    }
}
