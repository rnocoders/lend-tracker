package com.rnocoders.lendtracker.shared;

import lombok.*;
import org.springframework.http.*;
import org.springframework.stereotype.*;

@Data
@Component
public class ErrorMessage {

    private HttpStatus  httpStatus;
    private Integer statusCode;
    private String message;
}
